import roboticstoolbox
import math
import numpy

class myCobot(roboticstoolbox.DHRobot):
    def __init__(self):
        mm = 1e-3
        pi2 = math.pi / 2.0
        a = numpy.r_[0,0,-110.4,-96,0,0] * mm
        d = numpy.r_[13,0,0,64.62,73.18,48.6] * mm
        alpha = [0,pi2,0,0,pi2,-pi2]
        offset = [0,-pi2,0,-pi2,pi2,0]
        links = []

        for j in range(6):
            link = roboticstoolbox.RevoluteDH(d=d[j], a=a[j], alpha=alpha[j], offset=offset[j])
            links.append(link)

        super().__init__(
            links,
            name="myCobot 280 Pi - 2023",
            manufacturer="Elephant Robotics",
        )

        self.qr = numpy.zeros(6)
        self.qz = numpy.zeros(6)

        self.addconfiguration("qr", self.qr)
        self.addconfiguration("qz", self.qz)

if __name__ == "__main__":

    mycobot = myCobot()
    print(mycobot)

