import time
import pymycobot

def WaitForUp():
  Up = False
  while not Up:
    Up = mc.is_controller_connected()
    mc.clear_error_information()

def WaitForMovement():
  time.sleep(2)
  return
  mc.set_color(0,0,0)
  Moving = True
  while Moving:
    Moving = mc.is_moving()
    mc.set_color(0,0,255)
    print(Moving)

mc = pymycobot.MyCobot(pymycobot.PI_PORT,pymycobot.PI_BAUD)
print('Powering up')
mc.power_on()
WaitForUp()
mc.set_color(0,0,255)

print('Starting first movement')
mc.send_angles([0,0,0,0,0,0],20)
WaitForMovement()

print('Starting second movement')
mc.send_angles([0,-30,-30,-30,0,0],20)
WaitForMovement()

print('Starting third movement')
mc.send_angles([15,-30,-30,-30,15,15],20)
WaitForMovement()

print('Starting fourth movement')
mc.send_angles([-15,-30,-30,-30,-15,-15],20)
WaitForMovement()

print('Starting fifth movement')
mc.send_angles([0,-30,-30,-30,0,0],20)
WaitForMovement()

print('Starting movement')
mc.send_angles([15,-30,-30,-30,0,15],20)
WaitForMovement()

print('Starting movement')
mc.send_angles([-15,-30,-30,-30,0,-15],20)
WaitForMovement()

print('Starting movement')
mc.send_angles([0,-30,-30,-30,0,0],20)
WaitForMovement()

mc.send_angles([0,0,0,0,0,0],20)
WaitForMovement()

mc.send_angles([30,0,0,0,0,0],20)
WaitForMovement()

mc.send_angles([60,0,0,0,0,0],20)
WaitForMovement()

mc.send_angles([60,0,-30,0,0,0],20)
WaitForMovement()

mc.send_angles([0,-30,-30,-30,0,0],20)
WaitForMovement()

mc.set_color(0,255,0)
