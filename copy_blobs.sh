#!/bin/bash -ex

if [ ! -e blobs-rootfs ]; then
	git clone -b boot-pi git@openbeagle.org:beagley-ai/blobs-rootfs
fi

cd blobs-rootfs
git pull

BOOTNAME="/boot/firmware/" ROOTNAME="/" ./copy_blobs.sh
