import detection
import picknplace
import led
import threading
import time

arm_thread = None
arm_active = False
def start_drop(dest):
    global arm_thread, arm_active
    arm_active = True
    if arm_thread != None and arm_thread.is_alive():
        print('arm_thread is already running')
        return
    arm_thread = threading.Thread(target=do_drop,args=[dest])
    arm_thread.start()

def do_drop(dest):
    global arm_active
    picknplace.pickndrop(dest)
    time.sleep(3)
    arm_active = False

def main():
    picknplace.camera()
    no_key = True
    while no_key:
        cap = detection.cam_display()

        while cap.isOpened():
            success, image = cap.read()
            if not success:
                cap.release()
                break

            result, image = detection.process_frame(success, image)
            detection.show(image)

            if detection.exit_key():
                no_key = False
                break

            if arm_active:
                continue
            if result == None:
                continue
            if result == 1:
                print('Drop in 1st bin')
                led.red()
                start_drop(0)
                continue
            if result == 3:
                print('Drop in 3rd bin')
                led.green()
                start_drop(2)
                continue
            if result == 2:
                print('Drop in 2nd bin')
                led.yellow()
                start_drop(1)
                continue
            led.thinking()

    detection.cleanup(cap)

if __name__ == "__main__":
    main()
