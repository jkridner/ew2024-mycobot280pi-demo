import gpiod
import time

gpio20 = None
gpio21 = None
sInit = False

def Init():
    global sInit, gpio20, gpio21
    sInit = True
    gpio20 = gpiod.find_line('GPIO20')
    gpio21 = gpiod.find_line('GPIO21')
    r = gpiod.line_request()
    r.consumer = 'ew2024demo'
    r.request_type = gpiod.line_request.DIRECTION_OUTPUT
    gpio20.request(r)
    gpio21.request(r)
    off()

def on():
    global sInit, gpio20, gpio21
    if not sInit:
        Init()
    gpio20.set_value(0)
    gpio21.set_value(0)

def off():
    global sInit, gpio20, gpio21
    if not sInit:
        Init()
    gpio20.set_value(1)
    gpio21.set_value(1)

if __name__ == "__main__":
    for i in range(3):
        print('suction on')
        on()
        time.sleep(3)
        print('suction off')
        off()
        time.sleep(3)
