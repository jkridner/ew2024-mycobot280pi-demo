# ew2024-mycobot280pi-demo

## Notes

```console
$ ssh -L 8888:localhost:8888 er@er.local
```

https://debian.beagle.cc/images/disksnap-20240217.img.xz

5c3fcc56b5d531fd0548a6131f1ab65d355bcb0e55a14c6a3b9b1245d671c713  disksnap-20240217.img.xz

### Definitions

* Bounding box: width 24", depth 18", height 18" (includes rotation change)
* Single power connection.
* Ethernet optional for debug or data feed from display integrated by TI.
* BeagleY-AI is inside the robot arm
* 7" display connected via HDMI and USB

### Actions to finalize the demo
- [ ] Provide longer running time via handling a stack of cards

- [x] Rotate design 90 degrees so that the wide side is at the front of the table
- [ ] Design stickers
- [ ] Update documentation
  - [x] Shopping list
  - [ ] Setup instructions
  - [ ] Username and password (er:Elephant)
  - [ ] Source for "deck of cards"
  - [ ] Upload microSD image

### Scope for improvements

* TI (Reese) to work on increasing the inference speed and reducing the running power
* TI can provide additional "cards" that match the inference model

Link to demo code: https://openbeagle.org/beagley-ai/ew2024-mycobot280pi-demo

## Preparation

### Parts List

#### myCobot 280 Pi - 2023

* [Elephant Robotics myCobot 280 Pi - 2023](https://www.amazon.com/dp/B0B9W96SD6)
* [Elephant Robotics myCobot Suction Pump](https://www.amazon.com/dp/B09PH8YCVM)
* [Elephant Robotics myCobot Camera Flange](https://www.amazon.com/dp/B09PH867P6)
* [7" HDMI Display](https://www.amazon.com/dp/B09XKC53NH)
* [1ft microHDMI to HDMI cable](https://www.amazon.com/dp/B0B938BP37)
* [1ft USB micro-B male to USB type-A male](https://www.amazon.com/dp/B011KMSNXM)
* [18" x 24" Black Acrylic Sheet](https://www.amazon.com/dp/B0C85GHG11)
* [M2.5 Hardware](https://www.amazon.com/dp/B0BLCFD9HR)
* [M4 Hardware](https://www.amazon.com/dp/B09TYR314X)
* [Rubber Feet](https://www.amazon.com/dp/B0CB1BGFCQ)
* Stickers

#### myCobot 280 AI Kit - 2023



## Putting it together

### Verify Robot Arm Function

TODO: describe how to run the various tests on stock setup

### Modify Robot Arm to use BeagleY-AI

The robot arm is designed for a Pi 4, not a Pi 5. BealgeY-AI is mechanicially similar
to Pi 5.

BeagleY-AI EVT samples also have some large capacitors on the bottom side that need some
slight additional clearance.

TODO: Put images of the various cuts I made with my Dremel

Add steps for reassembly

### Cut and Assemble Plate

TODO: describe laser cutting the plate

TODO: describe where screws go and what goes where

TODO: print cards

### Modify microSD Card for BeagleY-AI

Pull the microSD card from the Pi 4 and attach it to a host computer running Linux.

```
copy_blobs
```

### Install demo

Connect Ethernet cable, microSD card, power cable and flip switch on.

Password: Elephant

```
host$ ssh er@er.local
er@er.local's password: Elephant^M
er@er:~$ ^D
```

Use public-key login (probably should also disable password login, but I didn't do that yet)

```
host$ ssh-copy-id er@er.local
host$ ssh er@er.local
er@er.local's password: Elephant^M
er@er:~$ sudo sed -i -e "s/PubkeyAuthentication no/PubkeyAuthentication yes/" /etc/ssh/sshd_config
er@er:~$ sudo systemctl reload sshd
er@er:~$ ^D
host$ ssh er@er.local
er@er:~$
```

Copy keys for accessing git repos

```
host$ scp /home/jkridner/.ssh/id_rsa* er@er.local:.ssh/
```

Clone demo code

```
er@er:~$ git clone git@openbeagle.org:beagley-ai/ew2024-mycobot280pi-demo
Cloning into 'ew2024-mycobot280pi-demo'...
The authenticity of host 'openbeagle.org (44.226.162.25)' can't be established.
ECDSA key fingerprint is SHA256:azBaSYRyGrNXo1cJ+G88gChJePMbL+YbWlDRb4aLhzQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'openbeagle.org,44.226.162.25' (ECDSA) to the list of known hosts.
Enter passphrase for key '/home/er/.ssh/id_rsa': MySecretPubKeyPassword^M
remote: Enumerating objects: 270, done.
remote: Counting objects: 100% (258/258), done.
remote: Compressing objects: 100% (255/255), done.
remote: Total 270 (delta 136), reused 0 (delta 0), pack-reused 12
Receiving objects: 100% (270/270), 56.70 MiB | 5.24 MiB/s, done.
Resolving deltas: 100% (139/139), done.
Updating files: 100% (39/39), done.
```

Install dependencies

```
er@er:~$ cd ew2024-mycobot280pi-demo
er@er:~/ew2024-mycobot280pi-demo$ sudo apt update
...
er@er:~/ew2024-mycobot280pi-demo$ sudo apt install -y python3-venv gpiod
...
er@er:~/ew2024-mycobot280pi-demo$ . ./venv-build-env.sh
...
(.venv) er@er:~/ew2024-mycobot280pi-demo$ ./get_models.sh 
...
```

Make sure you can talk to the robot arm

```
(.venv) er@er:~/ew2024-mycobot280pi-demo$ ./fixups.sh
(.venv) er@er:~/ew2024-mycobot280pi-demo$ python led.py
```

Set down some cards and put one in each bin

```
(.venv) er@er:~/ew2024-mycobot280pi-demo$ python picknplace.py
```

### Verify Demo

TODO: describe how to run the various tests on BeagleY-AI

### Finalize Demo

TODO: describe how to run the installer

TODO: descre

## Running the demo

TODO: describe how to execute the demo
