import model
import move
import json
import find_angles

def PlayAngles():
    with open('play_angles.json','r') as f:
        points = json.load(f)

    move.PowerUp()
    speed = 80

    for p in points:
        print('Moving to point ' + p["name"] + ' at ' + str(p["coords"]))
        move.MoveAbs(p["angles"],speed)

def PlayPoseValues():
    with open('play_angles.json','r') as f:
        points = json.load(f)

    for p in points:
        pose = find_angles.FindPose(p["angles"])
        print(p["name"] + ' at ' + str(p["coords"]) + ' has pose value of ' + str(pose))

if __name__ == "__main__":
    PlayAngles()
