import time
import pymycobot
import numpy

mc = pymycobot.MyCobot(pymycobot.PI_PORT,pymycobot.PI_BAUD)
step = 50.0

on_state = False
def on():
    global on_state
    if on_state:
        print('Already on')
    else:
        print('Powering up')
        mc.power_on()
        on_state = True
    wait_for_up()

def off():
    global on_state
    if on_state:
        print('Powering off')
        mc.power_off()
        on_state = False
    else:
        print('Already off')

def wait_for_up():
    mc.resume()
    up = False
    while not up:
        up = mc.is_controller_connected()
        mc.clear_error_information()

def init():
    print('Setting initial position')
    mc.sync_send_angles([0,0,0,-90,0,0],80,timeout=15)
    save_head()

def distance(a,b):
    x = numpy.array(a)
    y = numpy.array(b)
    return numpy.linalg.norm(x - y)

maCount = 0
def angles(a,s,timeout=7):
    global maCount
    if not on_state:
        on()
    maCount = maCount + 1
    print('move.angles #' + str(maCount) + ' to ' + str(a) + ' at speed ' + str(s))
    mc.sync_send_angles(a,s,timeout)
    return
    mc.set_color(0,255,255)
    start = time.time()
    mc.send_angles(a, s)
    while True:
        if time.time() - start > timeout:
            print('move.angles timed out')
            mc.set_color(255,0,0)
            return False 
        mc.pause()
        b = mc.get_angles()
        mc.resume()
        if b is None:
            err = mc.get_error_information()
            #print('get_angles() returned None: ' + str(err))
            continue
        m = mc.is_moving()
        try:
            d = distance(a,b)
        except Exception as err:
            print('Error: ' + str(err) + ' a = ' + str(a) + ' b = ' + str(b))
            continue
        print(str(b) + ': ' + str(m) + ' distance = ' + str(d))
        if m == 0:
        #    mc.set_color(0,0,255)
        #    return True
            mc.send_angles(a, s)
        if d < step:
            mc.set_color(0,0,255)
            return True
        time.sleep(0.5)

mrCount = 0
def MoveRel(c,s,timeout=7):
    global mrCount
    mrCount = mrCount + 1
    print('MoveRel #' + str(mrCount) + ' with delta ' + str(c) + ' at speed ' + str(s))
    mc.set_color(0,255,255)
    start = time.time()
    while True:
        if time.time() - start > timeout:
            print('MoveRel timed out')
            mc.set_color(255,0,0)
            return False 
        mc.pause()
        c0 = mc.get_coords()
        mc.resume()
        if c0:
            break
    c1 = c0.copy()
    c1[0] = c0[0] + c[0]
    c1[1] = c0[1] + c[1]
    c1[2] = c0[2] + c[2]
    l = distance(c0,c1)
    dx = (c[0]/l)*step
    dy = (c[1]/l)*step
    dz = (c[2]/l)*step
    print('MoveRel #' + str(mrCount) + ' from ' + str(c0) + ' to ' + str(c1) + ' a distance of ' + str(l))
    doneMakingCoords = False
    cX = c0.copy()
    while True:
        d = distance(cX,c1)
        if d > 2*step:
            cX[0] = cX[0] + dx
            cX[1] = cX[1] + dy
            cX[2] = cX[2] + dz
            print('MoveRel #' + str(mrCount) + ' added waypoint ' + str(cX))
        else:
            cX = c1.copy()
            doneMakingCoords = True
            print('MoveRel #' + str(mrCount) + ' added destination ' + str(cX))
        mc.send_coords(cX,s,1)
        if doneMakingCoords:
            break
    while True:
        if time.time() - start > timeout:
            print('MoveRel timed out')
            mc.set_color(255,0,0)
            return False 
        mc.pause()
        cX = mc.get_coords()
        mc.resume()
        if cX is None:
            err = mc.get_error_information()
            #print('get_coords() returned None: ' + str(err))
            continue
        m = mc.is_moving()
        d = distance(cX,c1)
        print(str(cX) + ': ' + str(m) + ' distance = ' + str(d))
        if d < step:
            mc.set_color(0,0,255)
            return True
        if m == 0:
        #    mc.set_color(0,0,255)
        #    return True
            mc.send_coords(c1,s,1)
        time.sleep(0.5)                   

mcCount = 0
def MoveCoord(c,s,timeout=7):
    global mcCount
    mcCount = mcCount + 1
    print('MoveCoord #' + str(mcCount) + ' to ' + str(c) + ' at speed ' + str(s))
    mc.set_color(0,255,255)
    start = time.time()
    while True:
        if time.time() - start > timeout:
            print('MoveCoord timed out')
            mc.set_color(255,0,0)
            return False 
        mc.pause()
        c0 = mc.get_coords()
        mc.resume()
        if c0:
            break
    c0[3] = headRX
    c0[4] = headRY
    c0[5] = headRZ
    c1 = c0.copy()
    c1[0] = c[0]
    c1[1] = c[1]
    c1[2] = c[2]
    #mc.sync_send_coords(c1,s,1,timeout=timeout)
    #return
    l = distance(c0,c1)
    dx = ((c1[0]-c0[0])/l)*step
    dy = ((c1[1]-c0[1])/l)*step
    dz = ((c1[2]-c0[2])/l)*step
    print('MoveCoord #' + str(mcCount) + ' from ' + str(c0) + ' to ' + str(c1) + ' a distance of ' + str(l))
    doneMakingCoords = False
    cX = c0.copy()
    while True:
        d = distance(cX,c1)
        if d > 2*step:
            cX[0] = cX[0]+dx
            cX[1] = cX[1]+dy
            cX[2] = cX[2]+dz
            print('MoveCoord #' + str(mcCount) + ' added waypoint ' + str(cX))
        else:
            cX = c1.copy()
            doneMakingCoords = True
            print('MoveCoord #' + str(mcCount) + ' added destination ' + str(cX))
        mc.send_coords(cX,s,0)
        if doneMakingCoords:
            break
    while True:
        if time.time() - start > timeout:
            print('MoveCoord timed out')
            mc.set_color(255,0,0)
            return False 
        mc.pause()
        cX = mc.get_coords()
        mc.resume()
        if cX is None:
            err = mc.get_error_information()
            #print('get_coords() returned None: ' + str(err))
            continue
        m = mc.is_moving()
        d = distance(cX,c1)
        print(str(cX) + ': ' + str(m) + ' distance = ' + str(d))
        if d < step:
            mc.set_color(0,0,255)
            return True
        if m == 0:
        #    mc.set_color(0,0,255)
        #    return True
            mc.send_coords(c1,s,1)
        time.sleep(0.5)

headRX = None
headRY = None
headRZ = None
def save_head(timeout=3):
    global headRX, headRY, headRZ
    start = time.time()
    while True:
        if time.time() - start > timeout:
            return False 
        mc.pause()
        c = mc.get_coords()
        mc.resume()
        if c:
            headRX = c[3]
            headRY = c[4]
            headRZ = c[5]

def demo():
    mc.set_fresh_mode(1)
    angles([0,-30,-60,0,0,0],80,timeout=15)
    mc.set_fresh_mode(0)
    time.sleep(0.5)
    #MoveRel([0,0,50],20)
    #MoveRel([50,0,0],20)
    xmin = 150
    xmax = 250
    ymin = -100
    ymax = 100
    z = 150
    MoveCoord([200,0,z],50,timeout=15)
    #angles(x200_y0_z150,80)
    for n in range(5):
        #MoveCoord([xmin,ymin,z],80,timeout=3)
        #MoveCoord([xmax,ymin,z],80,timeout=3)
        #MoveCoord([xmax,ymax,z],80,timeout=3)
        #MoveCoord([xmin,ymax,z],80,timeout=3)
        #angles(x150_yn100_z150,80)
        #angles(x250_yn100_z150,80)
        #angles(x250_y100_z150,80)
        #angles(x150_y100_z150,80)
        mc.send_angles(x150_yn100_z150,80)
        time.sleep(1.5)
        mc.send_angles(x250_yn100_z150,80)
        time.sleep(1.5)
        mc.send_angles(x250_y100_z150,80)
        time.sleep(1.5)
        mc.send_angles(x150_y100_z150,80)
        time.sleep(1.5)
    mc.set_fresh_mode(1)
    MoveCoord([200,0,z],50,timeout=15)

if __name__ == "__main__":
    on()
    init()
    demo()
