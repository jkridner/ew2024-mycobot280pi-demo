import move
import suction
import time
import points

if False:
    s_camera = 'camera'
    s_prepick = 'camera'
    s_pick = 'pick'
    s_up = 'up'
    s_bin_a = 'bin_a'
    s_bin_b = 'bin_b'
    s_bin_c = 'bin_c'
    s_bin_d = 'bin_c'
else:
    s_camera = 'ai camera view'
    s_prepick = 'ai prepickup'
    s_pick = 'ai pickup'
    s_up = 'ai prepickup'
    s_bin_a = 'ai bin a'
    s_bin_b = 'ai bin b'
    s_bin_c = 'ai bin c'
    s_bin_d = 'ai bin d'

bins = [s_bin_a, s_bin_b, s_bin_c, s_bin_d]

def camera():
    move.angles(points.a(s_camera), 80)

def pickndrop(dest):
    move.angles(points.a(s_prepick), 80)
    suction.on()
    move.angles(points.a(s_pick), 50)
    time.sleep(1)
    move.angles(points.a(s_up), 20)
    time.sleep(1)
    move.angles(points.a(bins[dest]), 20)
    time.sleep(1)
    suction.off()
    time.sleep(1)
    move.angles(points.a(s_camera), 80)

if __name__ == "__main__":
    move.on()
    print('camera')
    camera()
    time.sleep(3)
    for i in range(4):
        print('pickndrop ' + str(i))
        pickndrop(i)
        time.sleep(3)
