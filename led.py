import move

mc = move.mc

def red():
    mc.set_color(255,0,0)

def yellow():
    mc.set_color(200,200,0)

def green():
    mc.set_color(0,255,0)

def cyan():
    mc.set_color(0,200,200)

def blue():
    mc.set_color(0,0,255)

def magenta():
    mc.set_color(200,0,200)

def white():
    mc.set_color(255,255,255)

def thinking():
    mc.set_color(150,150,255)

if __name__ == '__main__':
    import time
    print('white')
    white()
    time.sleep(1.5)
    print('green')
    green()
    time.sleep(1.5)
    print('yellow')
    yellow()
    time.sleep(1.5)
    print('red')
    red()
    time.sleep(1.5)
    print('fast cycle')
    for i in range(5):
        red()
        time.sleep(0.1)
        yellow()
        time.sleep(0.1)
        green()
        time.sleep(0.1)
        cyan()
        time.sleep(0.1)
        blue()
        time.sleep(0.1)
        magenta()
        time.sleep(0.1)
    print('thinking')
    thinking()
