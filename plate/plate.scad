module four_screws(x,y,r) {
    translate([-x/2,-y/2]) circle(r,$fn=100);
    translate([-x/2,y/2]) circle(r,$fn=100);
    translate([x/2,-y/2]) circle(r,$fn=100);
    translate([x/2,y/2]) circle(r,$fn=100);
}

module arm_screws(x,y,r) {
    translate([x,y])
    rotate([0,0,r])
    four_screws(44,44,2.25);
}

module arm_outline(x,y,r) {
    translate([x,y])
    rotate([0,0,r])
    difference() {
        circle(2*25.4+1);
        circle(2*25.4);
    }
}

module display(x,y,r) {
    translate([x,y])
    rotate([0,0,r])
    four_screws(6*25.4,4*25.4,2.75/2);
}

module bin(x,y,r) {
     translate([x,y])
     rotate([0,0,r])
     {
         translate([-(3.25/2.0)*25.4,-(3.25/2.0)*25.4])
         square([3.25*25.4,2]);
         translate([-(3.25/2.0)*25.4,(3.25/2.0)*25.4-2])
         square([3.25*25.4,2]);
     }
}

module wirestrap(x,y,r) {
    translate([x,y])
    rotate([0,0,r])
    {
        translate([0,-3])
        square([5,2],center=true);
        translate([0,3])
        square([5,2],center=true);
    }
}

module cuts() {
    arm_screws(9.75*25.4,9*25.4,0);
    display(7.5*25.4,3.5*25.4,90);
    display(7*25.4,4*25.4,0);
    bin(8.5*25.4,(18-2.75)*25.4,90);
    bin(13.5*25.4,(18-2.75)*25.4,90);
    bin(18.5*25.4,(18-2.75)*25.4,90);
    bin((24-2.25)*25.4,2.25*25.4,90);
    translate([0.5*25.4,7.5*25.4])
    difference() {
        square([3.75*25.4+1,3*25.4+1]);
        translate([0.5,0.5])
        square([3.75*25.4,3*25.4]);
    }
    wirestrap(8.5*25.4,12*25.4,0); // hdmi in back
    wirestrap(12.5*25.4,9*25.4,90); // hdmi on right
    wirestrap(8.5*25.4,6.5*25.4,0); // usb between display and arm
    wirestrap(2*25.4,12*25.4,0); // power on left
    wirestrap(2*25.4,16*25.4,90); // power in back
    wirestrap(4.75*25.4,11.5*25.4,90); // pump power
    wirestrap(6*25.4,13*25.4,45); // wrap outside
    wirestrap(5*25.4,14*25.4,45); // wrap inside
}

module etching() {
    arm_outline(9.75*25.4,9*25.4,0);
    translate([18.5*25.4,9*25.4])
    square([1,25],center=true);
    translate([18.5*25.4,9*25.4])
    rotate([0,0,90])
    square([1,25],center=true);
    translate([18.5*25.4,6*25.4])
    square([5*25.4,1],center=true);
    translate([18.5*25.4,12*25.4])
    square([5*25.4,1],center=true);
    translate([16*25.4,9*25.4])
    square([1,6*25.4],center=true);
    translate([21*25.4,9*25.4])
    square([1,6*25.4],center=true);
}

module gen3d() {
    color("black")
    difference() {
        cube([24*25.4,18*25.4,3]);
        union() {
            translate([0,0,-1])
            linear_extrude(height=5)
            cuts();
            translate([0,0,2.5])
            linear_extrude(height=1)
            etching();
        }
    }
}

module gen2d() {
    color("green")
    etching();
    square([24*25.4,18*25.4]); // outline
    color("black")
    cuts();
}

gen3d();