import move
import json
import numpy
import jsbeautifier

mc = move.mc

points = []
with open('points.json','r') as f:
    data = json.load(f)
    for point in data:
        points.append(point)

'''
Not yet working

def find_pose(a):
    r = numpy.deg2rad(a)
    pose = m.fkine(r)
    xyz = numpy.r_[pose.x, pose.y, pose.z] * 1e3
    angle = pose.angvec(unit='deg')
    return(xyz, angle)
'''

def record():
    global points
    mc.release_all_servos()

    print('Move to location and provide name or press <ENTER> to exit')
    while True:
        print('>>> ')
        p = {}
        p["name"] = input()
        if p["name"] == '':
            break
        while True:
            p["angles"] = mc.get_angles()
            p["coords"] = mc.get_coords()
            if p["angles"] and p["coords"]:
                break
        points.append(p)

    with open('points.json','w') as f:
        options = jsbeautifier.default_options()
        options.indent_size = 2
        f.write(jsbeautifier.beautify(json.dumps(points), options))

def show():
    mc.release_all_servos()
    while True:
        a = mc.get_angles()
        print("^C to exit >>> " + str(a) + ' '*(50-len(str(a))), end='\r')

def play(pts=None,speed=80):
    if pts == None:
        pts = points
    move.on()
    for p in pts:
        print('Moving to point ' + p["name"] + ' at ' + str(p["coords"]))
        move.angles(p["angles"],speed)

def a(name):
    for pt in points:
        if pt['name'] == name:
            return pt['angles']
    return None

if __name__ == '__main__':
    play()
