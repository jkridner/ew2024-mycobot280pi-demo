#!/bin/bash
if [ ! -e /dev/ttyAMA0 ]; then
	sudo ln -s /dev/ttyS3 /dev/ttyAMA0
fi
source ./venv-build-env.sh
export LD_PRELOAD=/usr/lib/aarch64-linux-gnu/libGLdispatch.so.0
export XAUTHORITY=/home/er/.Xauthority
export DISPLAY=:0.0
python detection.py
