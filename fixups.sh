#!/bin/bash
# Need to move this to a udev rule
sudo chmod ugo+rwx /dev/gpiochip*
if [ ! -e /dev/ttyAMA0 ]; then
	sudo ln -s /dev/ttyS3 /dev/ttyAMA0
fi

# Setup background image
sudo mkdir -p /usr/share/backgrounds/bbb.io/
sudo cp beagleboard-logo.svg /usr/share/backgrounds/bbb.io/
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/bbb.io/beagleboard-logo.svg'
dconf write /org/mate/desktop/background/picture-filename "'/usr/share/backgrounds/bbb.io/beagleboard-logo.svg'"

# Remove clutter from the desktop
sudo mv $HOME/Desktop/*.desktop $HOME/
