import tflite_support
import tfutils
import cv2
import numpy

width = 800
height = 600
w = 400
h = 300
c = 250

detector = None
logo = None
mask = None
def init():
    global detector, logo, mask
    mask = numpy.zeros((height,width,3), numpy.uint8)
    cv2.rectangle(mask, (w-c,h-c), (w+c,h+c), (255,255,255), -1)
    logo = cv2.imread('beagleboard-logo-55.png', -1)
    add_logo(mask)
    model = '.models/efficientdet_lite0.tflite'
    base_options = tflite_support.task.core.BaseOptions(
        file_name=model, use_coral=False, num_threads=4)
    detection_options = tflite_support.task.processor.DetectionOptions(
        max_results=3, score_threshold=0.3)
    options = tflite_support.task.vision.ObjectDetectorOptions(
        base_options=base_options, detection_options=detection_options)
    detector = tflite_support.task.vision.ObjectDetector.create_from_options(options)

def process_frame(_, frame):
    if detector==None:
        init()
    if not _:
        return(None)
    cropped_image = crop_image(frame)
    rgb_image = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2RGB)
    input_tensor = tflite_support.task.vision.TensorImage.create_from_array(rgb_image)
    detection_result = detector.detect(input_tensor)
    color, result = process_result(detection_result)
    cropped_image = tfutils.visualize(cropped_image, detection_result, color=color)
    frame = combine_image(mask, cropped_image)
    return(result, frame)

green_list = ['dog']
yellow_list = ['cat', 'clock', 'scissors', 'zebra', 'motorcycle']
red_list = ['stop sign', 'banana', 'apple', 'orange', 'pizza']

def process_result(detection_result):
    color = (255,0,0) # blue
    result = 0
    for d in detection_result.detections:
        c = d.categories[0]
        name = c.category_name
        if name in green_list:
            color = (0,255,0) # green
            result = 3
            break
        elif name in red_list:
            color = (0,0,255) # red
            result = 1
            break
        elif name in yellow_list:
            color = (0,200,200) # yellow
            result = 2
            break
    return(color, result)

def add_mask(image):
    masked_image = cv2.bitwise_and(image, mask)
    return(masked_image)

def crop_image(image):
    cropped_image = image[h-c:h+c, w-c:w+c]
    return(cropped_image)

def combine_image(mask, image):
    combined_image = mask
    combined_image[h-c:h+c, w-c:w+c] = image
    return(combined_image)

def add_logo(image):
    alogo = logo[:,:,3] / 255.0
    aimage = 1.0 - alogo
    for c in range(0, 3):
        image[height-60:height-5,5:289,c] = alogo*logo[:,:,c] + aimage*image[height-60:height-5,5:289,c]

def cam_display():
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    cv2.namedWindow('BeagleY-AI', cv2.WINDOW_NORMAL)
    cv2.setWindowProperty('BeagleY-AI', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    return cap

def show(image):
    cv2.imshow('BeagleY-AI', image)

def exit_key():
    # Look for <ESC> key
    return(cv2.waitKey(1) == 27)

def cleanup(cap):
    cap.release()
    cv2.destroyAllWindows()

def main():
    cap = cam_display()

    while cap.isOpened():
        success, image = cap.read()
        if not success:
            sys.exit('ERROR: unable to read from webcam')

        result, image = process_frame(success, image)
        show(image)

        if exit_key():
            break

    cleanup(cap)

if __name__ == "__main__":
    main()
