#!/bin/bash
./get_models.sh
mkdir -p $HOME/.config/systemd/user
cp detectndrop.service $HOME/.config/systemd/user/
systemctl --user enable detectndrop.service
systemctl --user start detectndrop.service
